//  RemoteResource.h
//      Copyright (c) 2014 Kirill Bobkov.
//      Some rights reserved: http://opensource.org/licenses/MIT
//      https://bitbucket.org/kbobkov/remoteresource
//  Resource Host Address placed in Info.plist for key "RemoteResourcePath"

#import <Foundation/Foundation.h>
@interface RemoteResource : NSObject
@end
