//  RemoteResource.m
//      Copyright (c) 2014 Kirill Bobkov.
//      Some rights reserved: http://opensource.org/licenses/MIT
//      https://bitbucket.org/kbobkov/remoteresource

#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import "RemoteResource.h"
#import "JRSwizzle.h"

@interface RemoteResource()
@end
@implementation RemoteResource
+(NSString*)remoteUrl
{
    static NSString *urlString = nil;
    static dispatch_once_t guard;
    dispatch_once(&guard, ^{
        urlString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"RemoteResourcePath"];
        if( urlString == nil ) {
            NSLog( @"[RemoteResource] : !!! Resource path have not been defined !!!" );
            urlString = @"empty";
        }
        else {
            NSLog( @"[RemoteResource] : Path %@", urlString );
        }
    });
    return urlString;
}
@end


@implementation UIImage(RemoteResource)

+(void)load
{
    NSError *error;
    [UIImage jr_swizzleClassMethod:@selector(imageNamed:)
                   withClassMethod:@selector(imageNamed_swizzled:)
                             error:&error];
    if( error ) {
        NSLog( @"[RemoteResource] : %@",error );
    }
    
    [UIImage jr_swizzleClassMethod:objc_getClass("UIImageNibPlaceholder")
                                  :@selector(initWithCoder:)
                   withClassMethod:@selector(initWithCoder_swizzled:)
                             error:&error];
    if( error ) {
        NSLog( @"[RemoteResource] : %@",error );
    }
}

+(UIImage*)imageNamed_swizzled :(NSString*)name
{
    NSString *path = nil;
    NSString *remotePath = [RemoteResource remoteUrl];
    
    int aspect = (int)[[UIScreen mainScreen] scale];
    if( aspect == 1 ) {
        //  Not Retina Display
        path = [NSString stringWithFormat:@"%@%@",
                remotePath,
                name
                ];
    }
    else {
        //  Retina Display (@2x,@3x)
        NSString *pathExtension = [name pathExtension];
        path = [NSString stringWithFormat:@"%@%@@%@x.%@",
                remotePath,
                [name stringByDeletingPathExtension],
                [@(aspect) stringValue],
                ([pathExtension isEqualToString:@""] ? @"png" : pathExtension)
                ];
    }
    
    //  Try loading resource with @2x, @3x
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:path]];
    if( data == nil ) {
        //  Try load by default filename
        data = [NSData dataWithContentsOfURL:
                [NSURL URLWithString:[remotePath stringByAppendingString:name]]
                ];
    }
    
    UIImage *img = [UIImage imageWithData:data scale:aspect];
    if( img == nil ) {
        NSLog(@"[RemoteResource] : Error. Image %@ can not be found at remote host [%@]",
              name,
              remotePath
              );
        //  We call imageNamed_swizzled, because now it is an original method
        //  implementation after swizzling
        return [UIImage imageNamed_swizzled:name];
    }
    else {
        return img;
    }
}

//  New initWithCoder implementation for private UIImageNibPlaceholder
//  class
-(id)initWithCoder_swizzled:(NSCoder *)aDecoder
{
    //  Call swizzled imageNamed method
    NSString *name = [aDecoder decodeObjectForKey:@"UIResourceName"];
    return [UIImage imageNamed:name];
}

@end

